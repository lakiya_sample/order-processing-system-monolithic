package com.sample.monolithic.model;

public enum  OrderStatus {
    Pending, Accepted, Delivered;
}
