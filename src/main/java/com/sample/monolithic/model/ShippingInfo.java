package com.sample.monolithic.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "SHIPPING_INFO")
public class ShippingInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "Has Left Origin Country")
    private boolean leftOriginCountry;

    @ApiModelProperty(name = "Reached Destination Country")
    private boolean reachedDestinationCountry;

    @ApiModelProperty(name = "Is Same Day Shipping")
    private boolean sameDayShipping;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isLeftOriginCountry() {
        return leftOriginCountry;
    }

    public void setLeftOriginCountry(boolean leftOriginCountry) {
        this.leftOriginCountry = leftOriginCountry;
    }

    public boolean isReachedDestinationCountry() {
        return reachedDestinationCountry;
    }

    public void setReachedDestinationCountry(boolean reachedDestinationCountry) {
        this.reachedDestinationCountry = reachedDestinationCountry;
    }

    public boolean isSameDayShipping() {
        return sameDayShipping;
    }

    public void setSameDayShipping(boolean sameDayShipping) {
        this.sameDayShipping = sameDayShipping;
    }
}
