package com.sample.monolithic.model.dto;

import com.sample.monolithic.model.OrderStatus;

public class OrderUpdateDTO {
    private Long orderId;
    private OrderStatus orderStatus;
    private String itemName;
    private boolean leftOriginCountry;
    private boolean reachedDestinationCountry;
    private boolean sameDayShipping;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public boolean isLeftOriginCountry() {
        return leftOriginCountry;
    }

    public void setLeftOriginCountry(boolean leftOriginCountry) {
        this.leftOriginCountry = leftOriginCountry;
    }

    public boolean isReachedDestinationCountry() {
        return reachedDestinationCountry;
    }

    public void setReachedDestinationCountry(boolean reachedDestinationCountry) {
        this.reachedDestinationCountry = reachedDestinationCountry;
    }

    public boolean isSameDayShipping() {
        return sameDayShipping;
    }

    public void setSameDayShipping(boolean sameDayShipping) {
        this.sameDayShipping = sameDayShipping;
    }
}
