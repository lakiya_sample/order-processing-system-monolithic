package com.sample.monolithic.service;

import com.sample.monolithic.model.Customer;
import com.sample.monolithic.model.Order;
import com.sample.monolithic.model.OrderStatus;
import com.sample.monolithic.model.ShippingInfo;
import com.sample.monolithic.model.dto.OrderCreateDTO;
import com.sample.monolithic.model.dto.OrderUpdateDTO;
import com.sample.monolithic.repository.CustomerRepository;
import com.sample.monolithic.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository,
                            CustomerRepository customerRepository) {
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public Order createOrder(OrderCreateDTO orderCreateDTO) {
        Order order = new Order();
        Optional<Customer> optCustomer = this.customerRepository.findById(orderCreateDTO.getCustomerId());
        if(optCustomer.isPresent()){
            order.setCustomer(optCustomer.get());
            order.setItemName(orderCreateDTO.getItemName());
            order.setOrderStatus(OrderStatus.Pending);
            ShippingInfo shippingInfo = new ShippingInfo();
            shippingInfo.setLeftOriginCountry(false);
            shippingInfo.setReachedDestinationCountry(false);
            shippingInfo.setSameDayShipping(orderCreateDTO.isSameDayShipping());
            order.setShippingInfo(shippingInfo);
            this.orderRepository.save(order);
        }
        else{
            throw new RuntimeException("Customer not found with give id");
        }
        order.setOrderStatus(OrderStatus.Pending);
        return this.orderRepository.save(order);
    }

    @Override
    public Order updateOrder(OrderUpdateDTO orderUpdateDTO) {
        Optional<Order> optOrder = this.orderRepository.findById(orderUpdateDTO.getOrderId());
        if(optOrder.isPresent()){
            Order order = optOrder.get();
            order.setOrderStatus(orderUpdateDTO.getOrderStatus());
            order.setItemName(orderUpdateDTO.getItemName());
            ShippingInfo shippingInfo = order.getShippingInfo();
            shippingInfo.setSameDayShipping(orderUpdateDTO.isSameDayShipping());
            shippingInfo.setReachedDestinationCountry(orderUpdateDTO.isReachedDestinationCountry());
            shippingInfo.setLeftOriginCountry(orderUpdateDTO.isLeftOriginCountry());
            return this.orderRepository.save(order);
        }
        else {
            throw new RuntimeException("Order not found with give id");
        }
    }

    @Override
    public Order updateOrderStatus(Long orderId, OrderStatus orderStatus) {
        Optional<Order> optOrder = this.orderRepository.findById(orderId);
        if(optOrder.isPresent()){
            Order order = optOrder.get();
            order.setOrderStatus(orderStatus);
            return this.orderRepository.save(order);
        }
        else {
            throw new RuntimeException("Order not found with give id");
        }
    }
}
