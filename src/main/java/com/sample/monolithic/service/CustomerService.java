package com.sample.monolithic.service;

import com.sample.monolithic.model.Customer;
import com.sample.monolithic.model.dto.CustomerCreateDTO;
import org.springframework.stereotype.Component;

@Component
public interface CustomerService {
    Customer updateCustomer(Customer customer);

    Customer saveCustomer(CustomerCreateDTO customerCreateDTO);
}
