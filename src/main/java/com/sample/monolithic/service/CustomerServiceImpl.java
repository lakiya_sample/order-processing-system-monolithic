package com.sample.monolithic.service;

import com.sample.monolithic.model.Address;
import com.sample.monolithic.model.Customer;
import com.sample.monolithic.model.dto.CustomerCreateDTO;
import com.sample.monolithic.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        Optional<Customer> optionalCustomer = this.customerRepository.findById(customer.getId());
        if(optionalCustomer.isPresent()){
            Customer dbCustomer = optionalCustomer.get();
            dbCustomer.setFirstName(customer.getFirstName());
            dbCustomer.setLastName(customer.getLastName());
            Address address = dbCustomer.getAddress();
            if(address != null){
                address.setHouseNumber(customer.getAddress() != null ? customer.getAddress().getHouseNumber() : null);
                address.setStreetName(customer.getAddress() != null ? customer.getAddress().getStreetName() : null);
                dbCustomer.setAddress(address);
            }
            this.customerRepository.save(dbCustomer);
            return dbCustomer;
        }
        else {
            return null;
        }
    }

    @Override
    public Customer saveCustomer(CustomerCreateDTO customerCreateDTO) {
        Customer customer = new Customer();
        customer.setFirstName(customerCreateDTO.getFirstName());
        customer.setLastName(customerCreateDTO.getLastName());
        Address address = new Address();
        address.setStreetName(customerCreateDTO.getStreetName());
        address.setHouseNumber(customerCreateDTO.getHouseNumber());
        customer.setAddress(address);
        return this.customerRepository.save(customer);
    }
}
