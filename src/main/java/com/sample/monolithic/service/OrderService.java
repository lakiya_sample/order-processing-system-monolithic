package com.sample.monolithic.service;

import com.sample.monolithic.model.Order;
import com.sample.monolithic.model.OrderStatus;
import com.sample.monolithic.model.dto.OrderCreateDTO;
import com.sample.monolithic.model.dto.OrderUpdateDTO;
import org.springframework.stereotype.Component;

@Component
public interface OrderService {
    Order createOrder(OrderCreateDTO orderCreateDTO);

    Order updateOrder(OrderUpdateDTO orderUpdateDTO);

    Order updateOrderStatus(Long orderId, OrderStatus orderStatus);
}
