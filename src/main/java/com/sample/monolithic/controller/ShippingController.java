package com.sample.monolithic.controller;

import com.sample.monolithic.model.Order;
import com.sample.monolithic.model.ShippingInfo;
import com.sample.monolithic.repository.OrderRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("shipping")
public class ShippingController {
    private final OrderRepository orderRepository;

    public ShippingController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @GetMapping("{orderId}")
    public ResponseEntity<ShippingInfo> getOrderShippingInfo(@PathVariable Long orderId){
        Optional<Order> optOrder = this.orderRepository.findById(orderId);
        if(optOrder.isPresent()){
            return new ResponseEntity<>(optOrder.get().getShippingInfo(), HttpStatus.OK);
        }
        else{
            throw new RuntimeException("Order not found with give id");
        }
    }
}
