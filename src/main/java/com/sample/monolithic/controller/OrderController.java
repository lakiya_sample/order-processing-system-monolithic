package com.sample.monolithic.controller;

import com.sample.monolithic.model.Order;
import com.sample.monolithic.model.OrderStatus;
import com.sample.monolithic.model.dto.OrderCreateDTO;
import com.sample.monolithic.model.dto.OrderUpdateDTO;
import com.sample.monolithic.repository.OrderRepository;
import com.sample.monolithic.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController {
    private final OrderRepository orderRepository;
    private final OrderService orderService;

    public OrderController(OrderRepository orderRepository,
                           OrderService orderService) {
        this.orderRepository = orderRepository;
        this.orderService = orderService;
    }

    @PostMapping
    public ResponseEntity<Order> createOrder(@RequestBody OrderCreateDTO orderCreateDTO){
        Order dbOrder = this.orderService.createOrder(orderCreateDTO);
        return new ResponseEntity<>(dbOrder, HttpStatus.CREATED);
    }

    @GetMapping("{orderId}")
    public ResponseEntity<Order> findOrderById(@PathVariable Long orderId){
        Order order =  this.orderRepository.findById(orderId).orElse(null);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @GetMapping("searchByOrderStatus")
    public List<Order> findOrdersByStatus(@RequestParam OrderStatus orderStatus){
        return this.orderRepository.findByOrderStatus(orderStatus);
    }

    @PutMapping
    public ResponseEntity<Order> updateOrder(@RequestBody OrderUpdateDTO orderUpdateDTO){
        Order dbOrder = this.orderService.updateOrder(orderUpdateDTO);
        return new ResponseEntity<>(dbOrder, HttpStatus.OK);
    }

    @PutMapping("updateOrderStatus/{orderId}")
    public ResponseEntity<Order> updateOrderStatus(@PathVariable Long orderId, @RequestParam OrderStatus orderStatus){
        Order dbOrder = this.orderService.updateOrderStatus(orderId, orderStatus);
        return new ResponseEntity<>(dbOrder, HttpStatus.OK);
    }
}
