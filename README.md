# Order Processing System - Monolithic

Monolithic version of the order processing system

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Docker and
Linux Based environment or Windows environment that can run .sh

```
You can run .sh with git bash for windows
```

### Installing

Run the build.sh in project directory

```
./build.sh
```

Run the run.sh in project directory

```
./run.sh
```

## Access Swagger for API Documentation

[Swagger](http://localhost:8080/swagger-ui.html/) 